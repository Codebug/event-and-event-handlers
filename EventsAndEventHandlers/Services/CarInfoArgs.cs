﻿using EventsAndEventHandlers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndEventHandlers.Services
{
    public class CarInfoArgs : EventArgs
    {
        public Car Car;
        public CarInfoArgs(Car car) => Car = car;       
    }
}
