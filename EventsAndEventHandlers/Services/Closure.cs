﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventsAndEventHandlers.Services
{
    public class Closure
    {
        /// <summary>
        /// Implementation of Closure
        /// Defination: Closure is a feature that allows us access variables outside the block
        /// of a lambda expression.
        /// </summary>
        public static void DisplayResult()
        {
            Console.WriteLine("Example of closure");

            var input = Console.ReadLine();

            int numberInputed = 0;
            int.TryParse(input, out numberInputed);

            try
            {
                if(numberInputed == 0)
                {
                    Console.WriteLine("Please Enter a number");
                }

                Func<int, string> l = a =>
                {
                    var res = a * numberInputed;
                    return $"The number you inputed is {numberInputed}, when multiple by the constant passed you get {res} as result.";
                };

                Console.WriteLine(l(5));

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error Occured, Message: {ex.Message}");
            }
        }
    }
}
